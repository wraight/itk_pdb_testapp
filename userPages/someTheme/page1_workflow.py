### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################

infoList=["  * input componentType",
        "   * Get workflow: stages and tests (sankey plot)"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("Workflow", ":microscope: Plotted Stage Relationships", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        ### componentType code
        #infra.SelectBox(pageDict,'compType',compTypeRet,'Select componentType','name')
        infra.TextBox(pageDict,'compCode','Input ('+st.session_state.Authenticate['proj']['name']+') componentType *code*')
        if len(pageDict['compCode'])<1:
            st.stop()

        pageDict['compType']=DBaccess.DbGet(st.session_state.myClient,'getComponentTypeByCode',{'project':st.session_state.Authenticate['proj']['code'],'code':pageDict['compCode']})
        if st.session_state.debug:
            st.write("**DEBUG** Selected componentType")
            st.write(pageDict['compType'])
        if pageDict['compType']==None:
            st.write("No componentType dictionary found. Check inputs")
            st.stop()

        st.write("### Stage list")
        compStageList=[]
        for x in pageDict['compType']['stages']:
            compStageList.append({'name':x['name'],'code':x['code'],'order':x['order']})
            try:
                #print([y['testType'] for y in x['testTypes']])
                compStageList[-1]['testTypes']=[{'name':y['testType']['name'],'code':y['testType']['code']} for y in x['testTypes']]
            except TypeError:
                pass
            except KeyError:
                pass
        st.dataframe(compStageList)

        ### get lineage info.
        if st.button("get workflow!"):
            pageDict['workflow']={'label':[],'source':[],'target':[],'value':[]}
            stageCount=0
            count=0
            for cs in compStageList:
                pageDict['workflow']['label'].append(cs['name'])
                pageDict['workflow']['source'].append(stageCount)
                stageCount=len(pageDict['workflow']['label'])-1
                pageDict['workflow']['target'].append(stageCount)
                pageDict['workflow']['value'].append(0)
                try:
                    for t in cs['testTypes']:
                        pageDict['workflow']['source'].append(len(pageDict['workflow']['label']))
                        pageDict['workflow']['label'].append(t['name'])
                        pageDict['workflow']['target'].append(stageCount)
                        pageDict['workflow']['value'].append(1)
                except KeyError:
                    pass

        if "workflow" not in pageDict:
            st.write("no workflow set")
            st.stop()

        if st.session_state.debug:
            st.write(pageDict['workflow'])

        ### plotting
        st.write("### Plotting")
        fig = go.Figure(data=[go.Sankey(
                                orientation="h",
                                    node = dict(
                                      pad = 15,
                                      thickness = 20,
                                      line = dict(color = "black", width = 0.5),
                                      label = pageDict['workflow']['label'],
                                      color = "blue"
                                    ),
                                    link = dict(
                                      source = pageDict['workflow']['source'], # indices correspond to labels, eg A1, A2, A1, B1, ...
                                      target = pageDict['workflow']['target'],
                                      value = pageDict['workflow']['value']
                                  ))])

        fig.update_layout(title_text=pageDict['compCode']+"("+st.session_state.Authenticate['proj']['code']+")"" stages and tests", font_size=10)
        st.plotly_chart(fig)
