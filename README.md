# itk_pdb_testapp

Example of streamline webApp for ITk PDB interface.

### [Streamlit](https://www.streamlit.io) (python**3**) code for interfacing with ITk Production Database (PDB).

**NB** Powered by [itkdb](https://pypi.org/project/itkdb/)

Check requirements file for necessary libraries.

See [itk-web-apps README](https://gitlab.cern.ch/wraight/itk-web-apps/) for instructions on running via Docker.

---

## Overview

Add content to _userPages_ directory.

Example pages in _userPages/someTheme_ directory.

---

## Example ages

### Workflow
  * generate sankey plot for component stages and testTypes

### Test Data Upload
  * upload module test manually, based on PDB schema

### Inventory
  * generate inventory of components at institute

---

## Run Custom streamlit webApp

1. Run streamlit
> streamlit run mainApp.py

2. Open browser at ''localhost:8501''

Optional arguments:
  * set port: "--server.port=8501"
